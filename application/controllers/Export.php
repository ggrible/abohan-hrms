<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Export extends CI_Controller {
	// construct
    public function __construct() {
        parent::__construct();
    }    

    // export xlsx|xls file
    public function index() {
        echo 'test';
        $data['page'] = 'export-excel';
        $data['title'] = 'Export Excel data | TechArise';
        $this->load->view('export/index', $data);
    }

	// create xlsx
    public function createXLS() {
        $this->load->file(APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php");

        // create file name
        $fileName = 'data-'.time().'.xlsx';
                
        //$empInfo = $this->export->employeeList();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A1:AG1')
                    ->getCell('A1')
                    ->setValue('MAY ABOHAN FOOD CORP.');

        $objPHPExcel->getActiveSheet()
                    ->getStyle('A1')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A2:AG2')
                    ->getCell('A2')
                    ->setValue('TERMINAL Branch, Zone III, Sogod, Southern Leyte');

        $objPHPExcel->getActiveSheet()
                    ->getStyle('A2')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A3:AG3')
                    ->getCell('A3')
                    ->setValue('For The Payroll Period of NOVEMBER 26 - DECEMBER 10, 2017');

        $objPHPExcel->getActiveSheet()
                    ->getStyle('A3')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);            

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A6:F7')
                    ->getCell('A6')
                    ->setValue('NAMES');
        $objPHPExcel->getActiveSheet()
                    ->getStyle('A6')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);             

        $sameHeaderFormat = array(
                                'POSITION',
                                'DAYS WORK',
                                'BASIC PAY',
                                'COLA',
                                'REFUND',
                                'TRAVEL ALLOWANCE',
                                'HOL/OT PAY',
                                'ADDIT\'L COMPENSATION / BOARDING LODGING',
                                'LOAD ALLOWANCE',
                                'MEALS',
                                'GROSS EARNINGS'
                            );

        $startingPoint = 6;
        $endingPoint = 7;
        $startingLetterHead = 'G';

        foreach($sameHeaderFormat as $value) {
            $objPHPExcel->getActiveSheet()
                    ->mergeCells($startingLetterHead.$startingPoint.':'.$startingLetterHead.$endingPoint)
                    ->getCell($startingLetterHead.$startingPoint)
                    ->setValue($value);
            $objPHPExcel->getActiveSheet()
                    ->getStyle($startingLetterHead.$startingPoint)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
            $startingLetterHead++;   
        }

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('R6:AD6')
                    ->getCell('R6')
                    ->setValue('DEDUCTIONS');
        $objPHPExcel->getActiveSheet()
                    ->getStyle('R6')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        $sameHeaderFormat = array(
                                'PAG-IBIG',
                                'E-Share',
                                'PHIC',
                                'E-Share',
                                'SSS',
                                'E-Share',
                                'MEALS',
                                'PAG-IBIG',
                                'FINES (UNIFORM)',
                                'UT-Pay',
                                'WTAX',
                                'CHARGES',
                                'OTHERS'
                            );

        $startingPoint = 7;
        $startingLetterHead = 'R';

        foreach($sameHeaderFormat as $value) {
            $objPHPExcel->getActiveSheet()
                    ->mergeCells($startingLetterHead.$startingPoint.':'.$startingLetterHead.$endingPoint)
                    ->getCell($startingLetterHead.$startingPoint)
                    ->setValue($value);
            $objPHPExcel->getActiveSheet()
                    ->getStyle($startingLetterHead.$startingPoint)
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
 
            $startingLetterHead++;   
        }

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('AF6:AF7')
                    ->getCell('AF6')
                    ->setValue('TOTAL DEDUCTIONS');
        $objPHPExcel->getActiveSheet()
                    ->getStyle('AF6')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('AG6:AG7')
                    ->getCell('AG6')
                    ->setValue('NET PAY');
        $objPHPExcel->getActiveSheet()
                    ->getStyle('AG6')
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
           
        //$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
        //$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'DOB');
        //$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Contact_No');       
        // set Row
        $rowCount = 2;
        
        //foreach ($empInfo as $element) {
            // $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, 'Luffy');
            // $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, 'Rible');
            // $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, 'glenn.rible@gmail.com');
            // $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, 'April 18, 2016');
            // $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, '1234567');
//            $rowCount++;
//        }
        
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(FCPATH.'uploads/excel/'.$fileName);
		// download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url().'uploads/excel/'.$fileName);        
    }
    
}