<?php
/* Attendance Import view
*/
?>
<?php $session = $this->session->userdata('username');?>

<div class="row m-b-1">
  <div class="col-md-12">
    <div class="box box-block bg-white product-view mb-8">
          <h5>Import CSV file only</h5>
          <p class="font-100 text-muted mb-1">The first line in downloaded csv file should remain as it is. Please do not change the order of columns in csv file.</p>
          <h6><a href="<?php echo base_url();?>uploads/csv/sample-csv-attendance-format-1.csv" class="btn btn-info"> <i class="fa fa-download"></i> Download sample Format 1 File
          </a></h6>
          <h6><a href="<?php echo base_url();?>uploads/csv/sample-csv-attendance-format-2.csv" class="btn btn-info"> <i class="fa fa-download"></i> Download sample Format 2 File
          </a></h6>
          <div class="pv-form mt-2">
            <h6 class="mt-0">Upload File</h6>
            <form name="import_attendance" method="post" action="<?php echo site_url("timesheet/import_attendance"); ?>" id="xin-form" enctype="multipart/form-data">
                <span class="btn btn-primary btn-file">
                  Browse <input type="file" name="file" id="file">
                </span>
                <br>
                <small>Please select csv or excel file (allowed file size 500 KB)</small>
            	<div class="mt-1">
                <div>
                    <div class="form-group">
                        <select name="import_format" id="import_format" class="form-control" data-plugin="select_hrm" data-placeholder="Choose a format...">
                            <option value="">Select Format</option>`
                            <option value="format_1">Format 1</option>
                            <option value="format_2">Format 2</option>
                        </select>
                    </div>
                </div>
                <div class="mt-3">
                <button type="submit" class="btn btn-primary">Import</button>
            </div>
           </form> 
          </div>
        </div>
  </div>
</div>