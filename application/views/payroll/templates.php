<?php
/* Payroll Template view
*/
?>
<?php $session = $this->session->userdata('username');?>

<div class="add-form" style="display:none;">
  <div class="box box-block bg-white">
    <h2><strong>Setup</strong> Employee Payroll Template
      <div class="add-record-btn">
        <button class="btn btn-sm btn-primary add-new-form"><i class="fa fa-minus icon"></i> Hide</button>
      </div>
    </h2>
    <div class="row m-b-1">
      <div class="col-md-12">
        <form class="form-hrm" action="<?php echo site_url("payroll/add_template") ?>" method="post" name="add_template" id="xin-form" autocomplete="off">
          <input type="hidden" name="user_id" value="<?php echo $session['user_id'];?>">
          <div class="bg-white">
            <div class="box-block">
              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="salary_grades">Name of Employee</label>
                        <input class="form-control" placeholder="Name of Employee" name="salary_grades" type="text">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="basic_salary" class="control-label">Basic Salary</label>
                        <input class="form-control salary" placeholder="Basic Salary" name="basic_salary" type="number">
                      </div>
                    </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="meals_allowance" class="control-label">MEALS</label>
                          <input class="form-control salary allowance" placeholder="Amount" name="meals_allowance" type="number" value="">
                        </div>
                    </div>
<!--                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="overtime_rate" class="control-label">Overtime Rate ( Per Hour)</label>
                        <input class="form-control" placeholder="Overtime Rate ( Per Hour)" name="overtime_rate" type="text">
                      </div>
                    </div>-->
                  </div>
                </div>
              </div>
                <h2><strong>ALLOWANCES</strong></h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="overtime_rate" class="control-label">HOL/OT PAY</label>
                          <input class="form-control salary allowance" placeholder="Amount" name="overtime_rate" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="cola_allowance" class="control-label">COLA</label>
                          <input class="form-control salary allowance" placeholder="Amount" name="cola_allowance" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="refund_allowance" class="control-label">REFUND</label>
                          <input class="form-control salary allowance" placeholder="Amount" name="refund_allowance" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="travel_allowance" class="control-label">TRAVEL ALLOWANCE</label>
                          <input class="form-control salary allowance" placeholder="Amount" name="travel_allowance" type="number" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="add_boarding_allowance" class="control-label">ADDIT'L COMPENSATION / BOARDING LODGING</label>
                          <input class="form-control salary allowance" placeholder="Amount" name="add_boarding_allowance" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="load_allowance" class="control-label">LOAD ALLOWANCE</label>
                          <input class="form-control salary allowance" placeholder="Amount" name="load_allowance" type="number" value="">
                        </div>
                    </div>
                    
                </div>
                <h2><strong>DEDUCTIONS</strong></h2>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="pagibig_deduction" class="control-label">PAG-IBIG</label>
                          <input class="form-control deduction" placeholder="Amount" name="pagibig_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="eshare_pagibig_deduction" class="control-label">E-Share (PAG-IBIG)</label>
                          <input class="form-control deduction" placeholder="Amount" name="eshare_pagibig_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="phic_deduction" class="control-label">PHIC</label>
                          <input class="form-control deduction" placeholder="Amount" name="phic_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="eshare_phic_deduction" class="control-label">E-Share (PHIC)</label>
                          <input class="form-control deduction" placeholder="Amount" name="eshare_phic_deduction" type="number" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="sss_deduction" class="control-label">SSS</label>
                          <input class="form-control deduction" placeholder="Amount" name="sss_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="eshare_sss_deduction" class="control-label">E-Share (SSS)</label>
                          <input class="form-control deduction" placeholder="Amount" name="eshare_sss_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="meals_deduction" class="control-label">MEALS</label>
                          <input class="form-control deduction" placeholder="Amount" name="meals_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="fines_deduction" class="control-label">FINES (UNIFORM)</label>
                          <input class="form-control deduction" placeholder="Amount" name="fines_deduction" type="number" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="utpay_deduction" class="control-label">UT-Pay</label>
                          <input class="form-control deduction" placeholder="Amount" name="utpay_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="wtax_deduction" class="control-label">WTAX</label>
                          <input class="form-control deduction" placeholder="Amount" name="wtax_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="charges_deduction" class="control-label">CHARGES</label>
                          <input class="form-control deduction" placeholder="Amount" name="charges_deduction" type="number" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label for="others_deduction" class="control-label">OTHERS</label>
                          <input class="form-control deduction" placeholder="Amount" name="others_deduction" type="number" value="">
                        </div>
                    </div>
                </div>
              <div class="row">
                <div class="col-md-12 col-right">
                  <h2><strong>Total Salary Details</strong></h2>
                  <table class="table table-bordered custom-table">
                    <tbody>
                      <tr>
                        <th class="col-sm-4 vertical-td" style="text-align:right;">Gross Salary :</th>
                        <td class="hidden-print"><input type="text" name="gross_salary" readonly id="total" class="form-control"></td>
                      </tr>
                      <tr>
                        <th class="col-sm-4 vertical-td" style="text-align:right;">Total Allowance :</th>
                        <td class="hidden-print"><input type="text" name="total_allowance" readonly id="total_allowance" class="form-control"></td>
                      </tr>
                      <tr>
                        <th class="col-sm-4 vertical-td" style="text-align:right;">Total Deduction :</th>
                        <td class="hidden-print"><input type="text" name="total_deduction" readonly id="total_deduction" class="form-control"></td>
                      </tr>
                      <tr>
                        <th class="col-sm-4 vertical-td" style="text-align:right;">Net Salary :</th>
                        <td class="hidden-print"><input type="text" name="net_salary" readonly id="net_salary" class="form-control"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <button type="submit" class="btn btn-primary save primary-btn-block col-right">Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="box box-block bg-white">
  <h2><strong>List All</strong> Employees Payroll Templates
    <div class="add-record-btn">
      <button class="btn btn-sm btn-primary add-new-form"><i class="fa fa-plus icon"></i> Add New</button>
    </div>
  </h2>
  <div class="table-responsive" data-pattern="priority-columns">
    <table class="table table-striped table-bordered dataTable" id="xin_table" style="width:100%;">
      <thead>
        <tr>
          <th>Action</th>
          <th>Employee Name</th>
          <th>Basic Salary</th>
          <th>Net Salary</th>
          <th>Total Allowance</th>
          <th>Created By</th>
          <th>Created Date</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
