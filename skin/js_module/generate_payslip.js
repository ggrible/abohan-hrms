'use strict';
/** @type {!Array} */
var _$_3d0e = ["val", "#employee_id", "#month_year", "payroll/payslip_list/?employee_id=", "&month_year=", "GET", "tooltip", '[data-toggle="tooltip"]', "dataTable", "#xin_table", "data-options", "attr", "select2", '[data-plugin="select_hrm"]', "100%", "yy-mm", "1970:", "getFullYear", "hide-calendar", "addClass", "widget", "datepicker", "#ui-datepicker-div .ui-datepicker-month :selected", "#ui-datepicker-div .ui-datepicker-year :selected", "setDate", "removeClass", "hide", ".month_year", "preventDefault", 
"name", "POST", "action", "target", "serialize", "&is_ajax=2&form=", "error", "", "toggle", "modal", ".delete-modal", "result", "success", "reload", "ajax", "api", "submit", "#delete_record", "show.bs.modal", "relatedTarget", "employee_id", "data", "payroll/payroll_template_read/", "jd=1&is_ajax=11&mode=not_paid&data=payroll_template&type=payroll_template&employee_id=", "html", "#ajax_small_modal", "on", ".small-view-modal", "payroll/hourlywage_template_read/", "jd=1&is_ajax=11&mode=not_paid&data=hourlywages&type=hourlywages&employee_id=", 
"#ajax_small2_modal", ".small-view-modal-hr", "pay_id", "payroll/make_payment_view/", "jd=1&is_ajax=11&mode=modal&data=pay_payment&type=pay_payment&emp_id=", "&pay_id=", "#ajax_modal_details", ".detail_modal_data", "payroll/pay_monthly/", "jd=1&is_ajax=11&data=payment&type=monthly_payment&employee_id=", "&pay_date=", "#emo_monthly_pay_aj", ".emo_monthly_pay", "payroll/pay_hourly/", "jd=1&is_ajax=11&data=payment&type=hourly_payment&employee_id=", "#emo_hourly_pay_aj", ".emo_hourly_pay", "disabled", 
"prop", ".save", "show", ".icon-spinner3", "&is_ajax=1&edit_type=payroll&form=", "#user_salary_template", "#set_salary_details", "ready"];
$(document)[_$_3d0e[84]](function() {
  var search = jQuery(_$_3d0e[1])[_$_3d0e[0]]();
  var GROUP_UPDATE_INFO_URL = jQuery(_$_3d0e[2])[_$_3d0e[0]]();
  var c = $(_$_3d0e[9])[_$_3d0e[8]]({
    "bDestroy" : true,
    "ajax" : {
      url : site_url + _$_3d0e[3] + search + _$_3d0e[4] + GROUP_UPDATE_INFO_URL,
      type : _$_3d0e[5]
    },
    "fnDrawCallback" : function(oSettings) {
      $(_$_3d0e[7])[_$_3d0e[6]]();
    }
  });
  $(_$_3d0e[13])[_$_3d0e[12]]($(this)[_$_3d0e[11]](_$_3d0e[10]));
  $(_$_3d0e[13])[_$_3d0e[12]]({
    width : _$_3d0e[14]
  });
  $(_$_3d0e[27])[_$_3d0e[21]]({
    changeMonth : true,
    changeYear : true,
    showButtonPanel : true,
    dateFormat : _$_3d0e[15],
    yearRange : _$_3d0e[16] + (new Date)[_$_3d0e[17]](),
    beforeShow : function(obj) {
      $(obj)[_$_3d0e[21]](_$_3d0e[20])[_$_3d0e[19]](_$_3d0e[18]);
    },
    onClose : function(iItem, iButton) {
      var mm2 = $(_$_3d0e[22])[_$_3d0e[0]]();
      var interpretdYear = $(_$_3d0e[23])[_$_3d0e[0]]();
      $(this)[_$_3d0e[21]](_$_3d0e[24], new Date(interpretdYear, mm2, 1));
      $(this)[_$_3d0e[21]](_$_3d0e[20])[_$_3d0e[25]](_$_3d0e[18]);
      $(this)[_$_3d0e[21]](_$_3d0e[20])[_$_3d0e[26]]();
    }
  });
  $(_$_3d0e[46])[_$_3d0e[45]](function(canCreateDiscussions) {
    canCreateDiscussions[_$_3d0e[28]]();
    var $realtime = $(this);
    var showSliderNum = $realtime[_$_3d0e[11]](_$_3d0e[29]);
    $[_$_3d0e[43]]({
      type : _$_3d0e[30],
      url : canCreateDiscussions[_$_3d0e[32]][_$_3d0e[31]],
      data : $realtime[_$_3d0e[33]]() + _$_3d0e[34] + showSliderNum,
      cache : false,
      success : function(retu_data) {
        if (retu_data[_$_3d0e[35]] != _$_3d0e[36]) {
          toastr[_$_3d0e[35]](retu_data[_$_3d0e[35]]);
        } else {
          $(_$_3d0e[39])[_$_3d0e[38]](_$_3d0e[37]);
          c[_$_3d0e[44]]()[_$_3d0e[43]][_$_3d0e[42]](function() {
            toastr[_$_3d0e[41]](retu_data[_$_3d0e[40]]);
          }, true);
        }
      }
    });
  });
  $(_$_3d0e[56])[_$_3d0e[55]](_$_3d0e[47], function(o) {
    var $realtime = $(o[_$_3d0e[48]]);
    var showSliderNum = $realtime[_$_3d0e[50]](_$_3d0e[49]);
    var $gBCRBottom = $(this);
    $[_$_3d0e[43]]({
      url : site_url + _$_3d0e[51],
      type : _$_3d0e[5],
      data : _$_3d0e[52] + showSliderNum,
      success : function(htmlExercise) {
        if (htmlExercise) {
          $(_$_3d0e[54])[_$_3d0e[53]](htmlExercise);
        }
      }
    });
  });
  $(_$_3d0e[60])[_$_3d0e[55]](_$_3d0e[47], function(o) {
    var $realtime = $(o[_$_3d0e[48]]);
    var showSliderNum = $realtime[_$_3d0e[50]](_$_3d0e[49]);
    var $gBCRBottom = $(this);
    $[_$_3d0e[43]]({
      url : site_url + _$_3d0e[57],
      type : _$_3d0e[5],
      data : _$_3d0e[58] + showSliderNum,
      success : function(htmlExercise) {
        if (htmlExercise) {
          $(_$_3d0e[59])[_$_3d0e[53]](htmlExercise);
        }
      }
    });
  });
  $(_$_3d0e[66])[_$_3d0e[55]](_$_3d0e[47], function(o) {
    var $realtime = $(o[_$_3d0e[48]]);
    var a = $realtime[_$_3d0e[50]](_$_3d0e[49]);
    var showSliderNum = $realtime[_$_3d0e[50]](_$_3d0e[61]);
    var $gBCRBottom = $(this);
    $[_$_3d0e[43]]({
      url : site_url + _$_3d0e[62],
      type : _$_3d0e[5],
      data : _$_3d0e[63] + a + _$_3d0e[64] + showSliderNum,
      success : function(htmlExercise) {
        if (htmlExercise) {
          $(_$_3d0e[65])[_$_3d0e[53]](htmlExercise);
        }
      }
    });
  });
  $(_$_3d0e[71])[_$_3d0e[55]](_$_3d0e[47], function(o) {
    var $realtime = $(o[_$_3d0e[48]]);
    var a = $realtime[_$_3d0e[50]](_$_3d0e[49]);
    var showSliderNum = $(_$_3d0e[2])[_$_3d0e[0]]();
    var $gBCRBottom = $(this);
    $[_$_3d0e[43]]({
      url : site_url + _$_3d0e[67],
      type : _$_3d0e[5],
      data : _$_3d0e[68] + a + _$_3d0e[69] + showSliderNum,
      success : function(htmlExercise) {
        if (htmlExercise) {
          $(_$_3d0e[70])[_$_3d0e[53]](htmlExercise);
        }
      }
    });
  });
  $(_$_3d0e[75])[_$_3d0e[55]](_$_3d0e[47], function(o) {
    var $realtime = $(o[_$_3d0e[48]]);
    var a = $realtime[_$_3d0e[50]](_$_3d0e[49]);
    var showSliderNum = $(_$_3d0e[2])[_$_3d0e[0]]();
    var $gBCRBottom = $(this);
    $[_$_3d0e[43]]({
      url : site_url + _$_3d0e[72],
      type : _$_3d0e[5],
      data : _$_3d0e[73] + a + _$_3d0e[69] + showSliderNum,
      success : function(htmlExercise) {
        if (htmlExercise) {
          $(_$_3d0e[74])[_$_3d0e[53]](htmlExercise);
        }
      }
    });
  });
  $(_$_3d0e[82])[_$_3d0e[45]](function(canCreateDiscussions) {
    canCreateDiscussions[_$_3d0e[28]]();
    var $realtime = $(this);
    var showSliderNum = $realtime[_$_3d0e[11]](_$_3d0e[29]);
    $(_$_3d0e[78])[_$_3d0e[77]](_$_3d0e[76], true);
    $(_$_3d0e[80])[_$_3d0e[79]]();
    $[_$_3d0e[43]]({
      type : _$_3d0e[30],
      url : canCreateDiscussions[_$_3d0e[32]][_$_3d0e[31]],
      data : $realtime[_$_3d0e[33]]() + _$_3d0e[81] + showSliderNum,
      cache : false,
      success : function(retu_data) {
        if (retu_data[_$_3d0e[35]] != _$_3d0e[36]) {
          toastr[_$_3d0e[35]](retu_data[_$_3d0e[35]]);
          $(_$_3d0e[78])[_$_3d0e[77]](_$_3d0e[76], false);
          $(_$_3d0e[80])[_$_3d0e[26]]();
        } else {
          c[_$_3d0e[44]]()[_$_3d0e[43]][_$_3d0e[42]](function() {
            toastr[_$_3d0e[41]](retu_data[_$_3d0e[40]]);
          }, true);
          $(_$_3d0e[80])[_$_3d0e[26]]();
          $(_$_3d0e[78])[_$_3d0e[77]](_$_3d0e[76], false);
        }
      }
    });
  });
  $(_$_3d0e[83])[_$_3d0e[45]](function(canCreateDiscussions) {
    canCreateDiscussions[_$_3d0e[28]]();
    var $realtime = $(this);
    var j = $realtime[_$_3d0e[11]](_$_3d0e[29]);
    var search = jQuery(_$_3d0e[1])[_$_3d0e[0]]();
    var GROUP_UPDATE_INFO_URL = jQuery(_$_3d0e[2])[_$_3d0e[0]]();
    var t = $(_$_3d0e[9])[_$_3d0e[8]]({
      "bDestroy" : true,
      "ajax" : {
        url : site_url + _$_3d0e[3] + search + _$_3d0e[4] + GROUP_UPDATE_INFO_URL,
        type : _$_3d0e[5]
      },
      "fnDrawCallback" : function(oSettings) {
        $(_$_3d0e[7])[_$_3d0e[6]]();
      }
    });
    t[_$_3d0e[44]]()[_$_3d0e[43]][_$_3d0e[42]](function() {
    }, true);
  });
});